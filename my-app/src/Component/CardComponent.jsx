import React from 'react'

import { Button } from 'react-bootstrap';
export default function CardComponent({Shop}) {
    return (
      <div className="container">
        <div className="row">
          {Shop.map((pro) => (
            <div className="col">
              <div className="card" style={{ width: "15rem" }}>
                <img
                  src="https://i.pinimg.com/originals/dc/59/72/dc5972747f25d516118ffd7790fd9c09.jpg"
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">{pro.id}</h5>
                  <h5 className="card-title">{pro.name}</h5>
                  <Button  className="btn btn-primary" disabled variant={pro.isSelect ? 'danger': 'success'}>
                    {pro.isSelect? 'Unselect' : 'Select'}
                  </Button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }   

