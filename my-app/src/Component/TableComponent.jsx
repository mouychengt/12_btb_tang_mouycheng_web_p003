import React, { Component } from 'react'
import CardComponent from './CardComponent';
import { Button } from 'react-bootstrap';
export default class TableComponent extends Component {
  constructor(){
    super()
    this.state={
      data: [
        {
          id: 1,
          name: "Coca Cola",
          isSelect: false,
        },
        {
          id: 2,
          name: "Pepsi",
          isSelect: false,
        },
        {
          id: 3,
          name: "Sting",
          isSelect: false,
        },
        {
          id: 4,
          name: "Milk",
          isSelect: true,
        },
      ],

    }
  }
  onSelect(index) {
    let newData = this.state.data;
    newData[index].isSelect = !newData[index].isSelect;
    this.setState({
      newData,
    });
  }
  render() {
    return (
    
      <div className='container mt-5'>
          <h1 style={{textAlign:'center',color:'blue'}}>Welcome To My Shop</h1>
         <table className="table mt-5">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Product Name</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            {
              this.state.data.map((pro,index)=>(
              <tr key={pro.id}>
              <th scope="row">{pro.id}</th>
              <td>{pro.name}</td>
              <td>
               <Button onClick={()=>{
                this.onSelect(index);
              }} className="btn btn-primary" variant={pro.isSelect? 'danger': 'success'}>
                    {pro.isSelect? 'Unselect' : 'Select'}
                  </Button>
                </td>
            </tr>
              ))
            }
          </tbody>
        </table>
        <CardComponent Shop={this.state.data} />
      </div>
    )
  }
}
